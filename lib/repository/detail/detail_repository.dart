import 'package:meal_app/helper/http_request.dart';
import 'package:meal_app/helper/utils.dart';
import 'package:meal_app/model/home/meal_response.dart';

class DetailRepository {
  final network = Network();

  Future<MealResponse> getDetail({String idMeal}) async {
    var url = parseUrl("lookup.php?i=$idMeal");
    final response = await network.getRequest(url);
    if (response.statusCode >= 200 && response.statusCode < 300) {
      print(response.data);
      return MealResponse.fromJson(response.data);
    } else {
      return MealResponse(error: true);
    }
  }
}
