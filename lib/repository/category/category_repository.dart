import 'package:meal_app/helper/http_request.dart';
import 'package:meal_app/helper/utils.dart';
import 'package:meal_app/model/category/category_response.dart';

class CategoryRepository {
  final network = Network();

  Future<CategoryResponse> getCategories() async {
    var url = parseUrl("categories.php");
    final response = await network.getRequest(url);
    if (response.statusCode >= 200 && response.statusCode < 300) {
      print(response.data);
      return CategoryResponse.fromJson(response.data);
    } else {
      return CategoryResponse(error: true);
    }
  }
}
