// To parse this JSON data, do
//
//     final categoryResponse = categoryResponseFromJson(jsonString);

import 'dart:convert';

CategoryResponse categoryResponseFromJson(String str) =>
    CategoryResponse.fromJson(json.decode(str));

String categoryResponseToJson(CategoryResponse data) =>
    json.encode(data.toJson());

class CategoryResponse {
  CategoryResponse({this.categories, this.error});

  List<Category> categories;
  bool error;

  factory CategoryResponse.fromJson(Map<String, dynamic> json) =>
      CategoryResponse(
        categories: json["categories"] == null
            ? null
            : List<Category>.from(
                json["categories"].map((x) => Category.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "categories": categories == null
            ? null
            : List<dynamic>.from(categories.map((x) => x.toJson())),
      };
}

class Category {
  Category({
    this.idCategory,
    this.strCategory,
    this.strCategoryThumb,
    this.strCategoryDescription,
  });

  String idCategory;
  String strCategory;
  String strCategoryThumb;
  String strCategoryDescription;

  factory Category.fromJson(Map<String, dynamic> json) => Category(
        idCategory: json["idCategory"] == null ? null : json["idCategory"],
        strCategory: json["strCategory"] == null ? null : json["strCategory"],
        strCategoryThumb:
            json["strCategoryThumb"] == null ? null : json["strCategoryThumb"],
        strCategoryDescription: json["strCategoryDescription"] == null
            ? null
            : json["strCategoryDescription"],
      );

  Map<String, dynamic> toJson() => {
        "idCategory": idCategory == null ? null : idCategory,
        "strCategory": strCategory == null ? null : strCategory,
        "strCategoryThumb": strCategoryThumb == null ? null : strCategoryThumb,
        "strCategoryDescription":
            strCategoryDescription == null ? null : strCategoryDescription,
      };
}
