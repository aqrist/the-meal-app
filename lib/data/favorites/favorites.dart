import 'package:moor_flutter/moor_flutter.dart';
part 'favorites.g.dart';

class Favorites extends Table {
  TextColumn get idMeal => text()();
  IntColumn get id => integer().autoIncrement()();
  TextColumn get strMeal => text()();
}

@UseMoor(tables: [Favorites])
class AppDatabase extends _$AppDatabase {
  AppDatabase()
      : super(FlutterQueryExecutor.inDatabaseFolder(
            path: "db.sqlite", logStatements: true));
  int get schemaVersion => 1;
  Future<List<Favorite>> getAllFavorite() => select(favorites).get();
  Stream<List<Favorite>> watchAllFavorite() => select(favorites).watch();
  Future insertNewFavorite(Favorite favorite) =>
      into(favorites).insert(favorite);
  Future deleteFavorite(Favorite favorite) =>
      delete(favorites).delete(favorite);
}
