// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'favorites.dart';

// **************************************************************************
// MoorGenerator
// **************************************************************************

// ignore_for_file: unnecessary_brace_in_string_interps, unnecessary_this
class Favorite extends DataClass implements Insertable<Favorite> {
  final String idMeal;
  final int id;
  final String strMeal;
  Favorite({@required this.idMeal, @required this.id, @required this.strMeal});
  factory Favorite.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final stringType = db.typeSystem.forDartType<String>();
    final intType = db.typeSystem.forDartType<int>();
    return Favorite(
      idMeal:
          stringType.mapFromDatabaseResponse(data['${effectivePrefix}id_meal']),
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      strMeal: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}str_meal']),
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (!nullToAbsent || idMeal != null) {
      map['id_meal'] = Variable<String>(idMeal);
    }
    if (!nullToAbsent || id != null) {
      map['id'] = Variable<int>(id);
    }
    if (!nullToAbsent || strMeal != null) {
      map['str_meal'] = Variable<String>(strMeal);
    }
    return map;
  }

  FavoritesCompanion toCompanion(bool nullToAbsent) {
    return FavoritesCompanion(
      idMeal:
          idMeal == null && nullToAbsent ? const Value.absent() : Value(idMeal),
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      strMeal: strMeal == null && nullToAbsent
          ? const Value.absent()
          : Value(strMeal),
    );
  }

  factory Favorite.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return Favorite(
      idMeal: serializer.fromJson<String>(json['idMeal']),
      id: serializer.fromJson<int>(json['id']),
      strMeal: serializer.fromJson<String>(json['strMeal']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'idMeal': serializer.toJson<String>(idMeal),
      'id': serializer.toJson<int>(id),
      'strMeal': serializer.toJson<String>(strMeal),
    };
  }

  Favorite copyWith({String idMeal, int id, String strMeal}) => Favorite(
        idMeal: idMeal ?? this.idMeal,
        id: id ?? this.id,
        strMeal: strMeal ?? this.strMeal,
      );
  @override
  String toString() {
    return (StringBuffer('Favorite(')
          ..write('idMeal: $idMeal, ')
          ..write('id: $id, ')
          ..write('strMeal: $strMeal')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode =>
      $mrjf($mrjc(idMeal.hashCode, $mrjc(id.hashCode, strMeal.hashCode)));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is Favorite &&
          other.idMeal == this.idMeal &&
          other.id == this.id &&
          other.strMeal == this.strMeal);
}

class FavoritesCompanion extends UpdateCompanion<Favorite> {
  final Value<String> idMeal;
  final Value<int> id;
  final Value<String> strMeal;
  const FavoritesCompanion({
    this.idMeal = const Value.absent(),
    this.id = const Value.absent(),
    this.strMeal = const Value.absent(),
  });
  FavoritesCompanion.insert({
    @required String idMeal,
    this.id = const Value.absent(),
    @required String strMeal,
  })  : idMeal = Value(idMeal),
        strMeal = Value(strMeal);
  static Insertable<Favorite> custom({
    Expression<String> idMeal,
    Expression<int> id,
    Expression<String> strMeal,
  }) {
    return RawValuesInsertable({
      if (idMeal != null) 'id_meal': idMeal,
      if (id != null) 'id': id,
      if (strMeal != null) 'str_meal': strMeal,
    });
  }

  FavoritesCompanion copyWith(
      {Value<String> idMeal, Value<int> id, Value<String> strMeal}) {
    return FavoritesCompanion(
      idMeal: idMeal ?? this.idMeal,
      id: id ?? this.id,
      strMeal: strMeal ?? this.strMeal,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (idMeal.present) {
      map['id_meal'] = Variable<String>(idMeal.value);
    }
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (strMeal.present) {
      map['str_meal'] = Variable<String>(strMeal.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('FavoritesCompanion(')
          ..write('idMeal: $idMeal, ')
          ..write('id: $id, ')
          ..write('strMeal: $strMeal')
          ..write(')'))
        .toString();
  }
}

class $FavoritesTable extends Favorites
    with TableInfo<$FavoritesTable, Favorite> {
  final GeneratedDatabase _db;
  final String _alias;
  $FavoritesTable(this._db, [this._alias]);
  final VerificationMeta _idMealMeta = const VerificationMeta('idMeal');
  GeneratedTextColumn _idMeal;
  @override
  GeneratedTextColumn get idMeal => _idMeal ??= _constructIdMeal();
  GeneratedTextColumn _constructIdMeal() {
    return GeneratedTextColumn(
      'id_meal',
      $tableName,
      false,
    );
  }

  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn('id', $tableName, false,
        hasAutoIncrement: true, declaredAsPrimaryKey: true);
  }

  final VerificationMeta _strMealMeta = const VerificationMeta('strMeal');
  GeneratedTextColumn _strMeal;
  @override
  GeneratedTextColumn get strMeal => _strMeal ??= _constructStrMeal();
  GeneratedTextColumn _constructStrMeal() {
    return GeneratedTextColumn(
      'str_meal',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns => [idMeal, id, strMeal];
  @override
  $FavoritesTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'favorites';
  @override
  final String actualTableName = 'favorites';
  @override
  VerificationContext validateIntegrity(Insertable<Favorite> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id_meal')) {
      context.handle(_idMealMeta,
          idMeal.isAcceptableOrUnknown(data['id_meal'], _idMealMeta));
    } else if (isInserting) {
      context.missing(_idMealMeta);
    }
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id'], _idMeta));
    }
    if (data.containsKey('str_meal')) {
      context.handle(_strMealMeta,
          strMeal.isAcceptableOrUnknown(data['str_meal'], _strMealMeta));
    } else if (isInserting) {
      context.missing(_strMealMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  Favorite map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return Favorite.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  $FavoritesTable createAlias(String alias) {
    return $FavoritesTable(_db, alias);
  }
}

abstract class _$AppDatabase extends GeneratedDatabase {
  _$AppDatabase(QueryExecutor e) : super(SqlTypeSystem.defaultInstance, e);
  $FavoritesTable _favorites;
  $FavoritesTable get favorites => _favorites ??= $FavoritesTable(this);
  @override
  Iterable<TableInfo> get allTables => allSchemaEntities.whereType<TableInfo>();
  @override
  List<DatabaseSchemaEntity> get allSchemaEntities => [favorites];
}
