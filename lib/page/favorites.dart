import 'package:flutter/material.dart';
import 'package:meal_app/data/favorites/favorites.dart';
import 'package:meal_app/page/details.dart';

class FavoritesPage extends StatefulWidget {
  @override
  _FavoritesPageState createState() => _FavoritesPageState();
}

class _FavoritesPageState extends State<FavoritesPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Container(
          height: 700,
          width: double.infinity,
          child: StreamBuilder(
            stream: AppDatabase().watchAllFavorite(),
            builder: (context, AsyncSnapshot<List<Favorite>> snapshot) {
              return Padding(
                padding: const EdgeInsets.all(8.0),
                child: ListView.builder(
                  itemBuilder: (_, index) {
                    return Card(
                      color: Colors.orangeAccent,
                      child: ListTile(
                          leading: GestureDetector(
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => DetailsPage(
                                          idMeal:
                                              snapshot.data[index].idMeal)));
                            },
                            child: CircleAvatar(
                              child: Text('${index + 1}'),
                              radius: 20,
                            ),
                          ),
                          title: Text(snapshot.data[index].strMeal),
                          trailing: IconButton(
                            icon: Icon(Icons.delete_outline),
                            onPressed: () {
                              setState(() {
                                AppDatabase()
                                    .deleteFavorite(snapshot.data[index]);
                              });
                            },
                            color: Colors.red,
                          )),
                    );
                  },
                  itemCount: snapshot.data.length,
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
