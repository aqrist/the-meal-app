import 'package:flutter/material.dart';
import 'package:meal_app/blocs/bloc/detail_bloc/detail_bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meal_app/data/favorites/favorites.dart';
import 'package:meal_app/helper/constrants.dart';

class DetailsPage extends StatefulWidget {
  final String idMeal;

  const DetailsPage({Key key, this.idMeal}) : super(key: key);

  @override
  _DetailsPageState createState() => _DetailsPageState();
}

class _DetailsPageState extends State<DetailsPage> {
  @override
  void initState() {
    super.initState();
    context.read<DetailBloc>().add(GetDetail(query: widget.idMeal));
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      body: Column(
        children: [
          SizedBox(height: 17),
          Row(
            children: [
              IconButton(
                icon: Icon(Icons.arrow_back_ios),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
              SizedBox(),
              Expanded(
                child: Text(
                  'Meal Details',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                ),
              )
            ],
          ),
          BlocBuilder<DetailBloc, DetailState>(
            builder: (context, state) {
              if (state is DetailLoading) {
                return Center(
                  child: Center(
                    child: shimmerBox(double.infinity, 100),
                  ),
                );
              }
              if (state is DetailLoaded) {
                return Expanded(
                  child: ListView.builder(
                    itemCount: state.data.meals.length,
                    itemBuilder: (BuildContext context, int index) {
                      var data = state.data.meals[index];
                      return Padding(
                        padding: const EdgeInsets.all(15.0),
                        child: Column(
                          children: [
                            CircleAvatar(
                              radius: size.width / 3,
                              backgroundImage: NetworkImage(
                                data.strMealThumb,
                              ),
                            ),
                            SizedBox(height: 20),
                            Text(
                              data.strMeal,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 18),
                            ),
                            Text("Category : " + data.strCategory),
                            Text("Tags : " + data.strCategory),
                            SizedBox(height: 20),
                            Text(data.strInstructions),
                            SizedBox(height: 20),
                            RaisedButton.icon(
                                onPressed: () {
                                  setState(
                                    () {
                                      AppDatabase().insertNewFavorite(
                                        Favorite(
                                          idMeal: data.idMeal,
                                          strMeal: data.strMeal,
                                        ),
                                      );
                                    },
                                  );
                                },
                                icon: Icon(Icons.favorite),
                                label: Text('Add to Favorite'))
                          ],
                        ),
                      );
                    },
                  ),
                );
              }
              if (state is DetailError) {
                return Center(
                  child: Text("data nnot found / check your connection"),
                );
              }
              return SizedBox();
            },
          ),
        ],
      ),
    );
  }
}
