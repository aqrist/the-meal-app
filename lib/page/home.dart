import 'package:flutter/material.dart';
import 'package:meal_app/blocs/bloc/category_bloc/category_bloc.dart';
import 'package:meal_app/blocs/bloc/home_bloc/home_bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meal_app/helper/constrants.dart';
import 'package:meal_app/model/category/category_response.dart';
import 'package:meal_app/model/home/meal_response.dart';
import 'package:meal_app/page/details.dart';
import 'package:meal_app/page/favorites.dart';

class HomePage extends StatefulWidget {
  final ValueChanged<Meal> onPressed;

  const HomePage({Key key, this.onPressed}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  HomeBloc _homeBloc;

  @override
  void initState() {
    super.initState();
    context.read<CategoryBloc>().add(GetCategory());
    _homeBloc = BlocProvider.of<HomeBloc>(context);
    _homeBloc.add(GetHome(query: "beef"));
  }

  void _onCategoryChanged(String strCat) {
    BlocProvider.of<HomeBloc>(context).add(GetHome(query: strCat));
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;

    return BlocBuilder<HomeBloc, HomeState>(
      builder: (_, homeState) => Scaffold(
        backgroundColor: Colors.white,
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => FavoritesPage()));
          },
          child: Icon(Icons.favorite),
        ),
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: size.height / 15,
            ),
            // Title
            Padding(
              padding: const EdgeInsets.only(left: 15),
              child: Container(
                width: size.width / 1.8,
                child: Text(
                  'Simple way to find tasty food',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 22.0,
                  ),
                ),
              ),
            ),
            // Categories
            SizedBox(
              height: size.height / 15,
            ),
            BlocBuilder<CategoryBloc, CategoryState>(
              builder: (context, state) {
                if (state is CategoryLoaded) {
                  if (state.data == null) {
                    return SizedBox();
                  }
                  if (state.data.error != null) {
                    return SizedBox();
                  }
                  return Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 15),
                    child: Container(
                      width: size.width - 30,
                      height: 30,
                      child: state.data.categories.length == 0
                          ? Center(
                              child: Text(
                              "Category not found",
                              style: TextStyle(
                                color: Colors.black,
                              ),
                            ))
                          : ListView.builder(
                              physics: ClampingScrollPhysics(),
                              scrollDirection: Axis.horizontal,
                              itemCount: state.data.categories.length,
                              itemBuilder: (context, index) => buildCategory(
                                  state.data.categories[index], homeState)),
                    ),
                  );
                }
                return Container(
                  height: 30,
                  child: Center(
                    child: shimmerBox(double.infinity, 100),
                  ),
                );
              },
            ),
            // Search Bar
            // Padding(
            //   padding: const EdgeInsets.symmetric(horizontal: 15),
            //   child: Container(
            //     decoration: BoxDecoration(
            //       borderRadius: BorderRadius.all(
            //         Radius.circular(10),
            //       ),
            //       border: Border.all(
            //         width: 1,
            //         color: Colors.black12,
            //       ),
            //     ),
            //     height: 45,
            //     child: Padding(
            //       padding: const EdgeInsets.only(left: 20),
            //       child: Row(
            //         children: [
            //           Icon(Icons.search),
            //         ],
            //       ),
            //     ),
            //   ),
            // ),

            // BODY HERE

            Container(
              height: size.height / 1.7,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: BlocBuilder<HomeBloc, HomeState>(
                  builder: (context, state) {
                    if (state is HomeLoading) {
                      return Center(
                        child: Center(
                          child: shimmerBox(double.infinity, 100),
                        ),
                      );
                    }
                    if (state is HomeLoaded) {
                      return buildDataHomeList(state);
                    }
                    if (state is HomeError) {
                      return Center(
                        child: Text("data not found / check your connection"),
                      );
                    }
                    return SizedBox();
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget buildDataHomeList(HomeLoaded state) {
    var size = MediaQuery.of(context).size;

    return ListView.separated(
      scrollDirection: Axis.horizontal,
      itemCount: state.data.meals.length,
      separatorBuilder: (BuildContext context, int index) =>
          SizedBox(width: 10),
      itemBuilder: (BuildContext context, int index) {
        var data = state.data.meals[index];
        return Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8.0),
          child: GestureDetector(
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => DetailsPage(idMeal: data.idMeal)));
            },
            child: Container(
              width: size.width / 1.2,
              height: size.height,
              child: Stack(
                children: [
                  PositionedDirectional(
                    top: 10,
                    start: 30,
                    child: Container(
                      height: size.height / 2.2,
                      width: size.width / 1.5,
                      decoration: BoxDecoration(
                        color: Colors.pink[50],
                        borderRadius: BorderRadius.all(Radius.circular(18)),
                      ),
                    ),
                  ),
                  PositionedDirectional(
                    top: 10,
                    start: 10,
                    child: Container(
                        width: 200,
                        height: 200,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(
                              Radius.circular(
                                100,
                              ),
                            ),
                            color: Colors.pink[200])),
                  ),
                  PositionedDirectional(
                    child: CircleAvatar(
                      radius: 100,
                      backgroundImage: NetworkImage(
                        data.strMealThumb,
                      ),
                    ),
                  ),
                  PositionedDirectional(
                    top: size.height / 2.5,
                    start: 50,
                    child: Container(
                      width: size.width / 2,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            data.strMeal,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  Widget buildCategory(Category category, HomeState state) {
    return GestureDetector(
      onTap: () {
        _onCategoryChanged(category.strCategory);
      },
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 9.0),
        child: Text(category.strCategory,
            style: state != null
                ? (state is HomeLoaded)
                    ? state.selectedId == category.strCategory
                        ? selectedText
                        : notselectedText
                    : notselectedText
                : notselectedText),
      ),
    );
  }

  TextStyle get selectedText =>
      TextStyle(fontWeight: FontWeight.bold, color: Colors.pink);
  TextStyle get notselectedText => TextStyle(color: Colors.grey);
}
