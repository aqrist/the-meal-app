part of 'favorite_bloc.dart';

abstract class FavoriteEvent extends Equatable {
  const FavoriteEvent();

  @override
  List<Object> get props => [];
}

class PostFavorite extends FavoriteEvent {
  final String query;

  PostFavorite({this.query});
}
