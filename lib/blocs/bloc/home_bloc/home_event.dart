part of 'home_bloc.dart';

abstract class HomeEvent extends Equatable {
  const HomeEvent();

  @override
  List<Object> get props => [];
}

class GetHome extends HomeEvent {
  final String query;

  GetHome({this.query});
}
