part of 'home_bloc.dart';

abstract class HomeState extends Equatable {
  const HomeState();

  @override
  List<Object> get props => [];
}

class HomeInitial extends HomeState {}

class HomeLoading extends HomeState {}

class HomeError extends HomeState {}

class HomeLoaded extends HomeState {
  final MealResponse data;
  final String selectedId;

  HomeLoaded({this.data, this.selectedId});
  @override
  List<Object> get props => [selectedId, data];
}
