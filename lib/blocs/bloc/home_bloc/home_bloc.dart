import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meal_app/model/home/meal_response.dart';
import 'package:meal_app/repository/home/home_repository.dart';

part 'home_event.dart';
part 'home_state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  HomeBloc({this.repository}) : super(HomeInitial());
  final HomeRepository repository;

  @override
  Stream<HomeState> mapEventToState(
    HomeEvent event,
  ) async* {
    if (event is GetHome) {
      yield HomeLoading();
      try {
        var result = await repository.getMealHome(strCat: event.query);
        yield HomeLoaded(data: result, selectedId: event.query);
      } catch (e) {
        print(e);
        yield HomeError();
      }
    }
  }
}
