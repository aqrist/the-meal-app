part of 'detail_bloc.dart';

abstract class DetailState extends Equatable {
  const DetailState();

  @override
  List<Object> get props => [];
}

class DetailInitial extends DetailState {}

class DetailLoading extends DetailState {}

class DetailError extends DetailState {}

class DetailLoaded extends DetailState {
  final MealResponse data;
  final String selectedId;

  DetailLoaded({this.data, this.selectedId});
  @override
  List<Object> get props => [selectedId, data];
}
