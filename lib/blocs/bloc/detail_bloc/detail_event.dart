part of 'detail_bloc.dart';

abstract class DetailEvent extends Equatable {
  const DetailEvent();

  @override
  List<Object> get props => [];
}

class GetDetail extends DetailEvent {
  final String query;

  GetDetail({this.query});
}
