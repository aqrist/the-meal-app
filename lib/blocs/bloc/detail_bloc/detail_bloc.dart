import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meal_app/model/home/meal_response.dart';
import 'package:meal_app/repository/detail/detail_repository.dart';

part 'detail_event.dart';
part 'detail_state.dart';

class DetailBloc extends Bloc<DetailEvent, DetailState> {
  DetailBloc({this.repository}) : super(DetailInitial());
  final DetailRepository repository;

  @override
  Stream<DetailState> mapEventToState(
    DetailEvent event,
  ) async* {
    if (event is GetDetail) {
      yield DetailLoading();
      try {
        var result = await repository.getDetail(idMeal: event.query);
        yield DetailLoaded(data: result, selectedId: event.query);
      } catch (e) {
        print(e);
        yield DetailError();
      }
    }
  }
}
