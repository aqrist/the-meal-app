part of 'category_bloc.dart';

abstract class CategoryState extends Equatable {
  const CategoryState();

  @override
  List<Object> get props => [];
}

class CategoryInitial extends CategoryState {}

class CategoryLoading extends CategoryState {}

class CategoryError extends CategoryState {}

class CategoryLoaded extends CategoryState {
  final CategoryResponse data;
  final String selectedId;

  CategoryLoaded({this.data, this.selectedId});
  @override
  List<Object> get props => [selectedId, data];
}
