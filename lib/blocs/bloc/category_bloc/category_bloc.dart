import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meal_app/model/category/category_response.dart';
import 'package:meal_app/repository/category/category_repository.dart';

part 'category_event.dart';
part 'category_state.dart';

class CategoryBloc extends Bloc<CategoryEvent, CategoryState> {
  CategoryBloc({this.repository}) : super(CategoryInitial());
  final CategoryRepository repository;

  @override
  Stream<CategoryState> mapEventToState(
    CategoryEvent event,
  ) async* {
    if (event is GetCategory) {
      yield CategoryLoading();
      try {
        var result = await repository.getCategories();
        yield CategoryLoaded(data: result);
      } catch (e) {
        print(e);
        yield CategoryError();
      }
    }
  }
}
