import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

const kPrimaryColor = Color(0xff853133);
const String baseEndpoint = "https://www.themealdb.com/api/json/v1/1/";

final otpInputDecoration = InputDecoration(
    contentPadding: EdgeInsets.symmetric(vertical: 15),
    enabledBorder: outlineInputBorder(),
    focusedBorder: outlineInputBorder(),
    border: outlineInputBorder());

OutlineInputBorder outlineInputBorder() {
  return OutlineInputBorder(
      borderRadius: BorderRadius.circular(15),
      borderSide: BorderSide(color: Colors.grey));
}

Widget shimmerBox(double sWidht, double sHeight) {
  return SizedBox(
    width: sWidht,
    height: sHeight,
    child: Shimmer.fromColors(
      baseColor: Colors.grey[300],
      highlightColor: Colors.grey[100],
      child: Padding(
        padding: const EdgeInsets.only(left: 20, right: 20),
        child: Container(
          width: double.infinity,
          height: sHeight,
          color: Colors.white,
        ),
      ),
    ),
  );
}
