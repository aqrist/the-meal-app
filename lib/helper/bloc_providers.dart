import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meal_app/blocs/bloc/category_bloc/category_bloc.dart';
import 'package:meal_app/blocs/bloc/detail_bloc/detail_bloc.dart';
import 'package:meal_app/blocs/bloc/home_bloc/home_bloc.dart';
import 'package:meal_app/repository/category/category_repository.dart';
import 'package:meal_app/repository/detail/detail_repository.dart';
import 'package:meal_app/repository/home/home_repository.dart';

class BlocProviders {
  getProviders() {
    final homeRepository = HomeRepository();
    final categoryRepository = CategoryRepository();
    final detailRepository = DetailRepository();

    return [
      BlocProvider(
        create: (context) => HomeBloc(repository: homeRepository),
      ),
      BlocProvider(
        create: (context) => CategoryBloc(repository: categoryRepository),
      ),
      BlocProvider(
        create: (context) => DetailBloc(repository: detailRepository),
      ),
    ];
  }
}
