import 'dart:async';
import 'package:dio/dio.dart';

// DIO
Dio dio = Dio();

class Network {
  Future<dynamic> getRequest(String url) async {
    try {
      final response = await dio.get("$url").timeout(
            Duration(
              seconds: 20,
            ),
          );
      return response;
    } on TimeoutException catch (_) {
      throw Exception(_);
    }
  }

  Future<dynamic> postRequest(String url, dynamic body) async {
    try {
      final response = await dio.post(
        "$url",
        data: body,
      );
      return response;
    } on TimeoutException catch (_) {
      throw Exception(_);
    }
  }
}
